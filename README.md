# TrainingRecords

A [Neo4J](http://neo4j.com/)-backed [DropWizard](http://www.dropwizard.io/) service to associate people with training records.

## Details

Using Neo4J notation, the *schema* is

`
(:PERSON) -[:HAS]-> (:COURSE)
`

* PERSON has name, dateOfBirth, mobile, email properties.
* COURSE has name, description properties
* HAS has gained, expires properties.

People are identified by their name and their date of birth (so people with the same name and date of birth will be rejected).

Courses are identified by the course name alone (so multiple courses with the same name are disallowed).

All timestamps are internally stored by millisecond from the Java Epoch (1970-01-01T00:00:00Z).


## Building

```bash
$ mvn -version
Apache Maven 3.3.1 (cab6659f9874fa96462afef40fcf6bc033d58c1c; 2015-03-13T20:10:27+00:00)
Maven home: /opt/boxen/homebrew/Cellar/maven/3.3.1/libexec
Java version: 1.8.0_20, vendor: Oracle Corporation
Java home: /Library/Java/JavaVirtualMachines/jdk1.8.0_20.jdk/Contents/Home/jre
Default locale: en_US, platform encoding: UTF-8
OS name: "mac os x", version: "10.10.2", arch: "x86_64", family: "mac"

$ mvn package
```

## Running

```bash
$ java -jar target/TrainingDatabase-1.0.3-SNAPSHOT.jar server config/local.yml
$ curl http://localhost:8080/person
```

*(Minor bug - if you put a trailing slash on the URL above, it serves the page but messes all the links up)*

## Docker

I've added a Dockerfile to create an image.

```bash
$ docker build -t trainingrecords .
$ docker run -d -v `pwd`/config/local.yml:/mnt/trainingrecords-config/config.yml -p 9080:8080 trainingrecords
$ curl http://localhost:9080/person
```

## Importing data

```bash
$ java -jar target/TrainingDatabase-1.0.3-SNAPSHOT.jar import config/local.yml
```

There are 3 files for import of data - people, courses, and the people-courses cross-reference. Filenames are specified
in the configuration file, as is the import and export date formats.


Data should be in CSV format, with each field separated by a comma, and each record by a CRLF sequence. Import failures
(eg where a non-existant person links to a course, or a person links to a non-existant course) will throw exceptions.

*import/people.csv* :-

name           | dateOfBirth | email                      | mobile
---------------|-------------|----------------------------|-------------
Alice Aardvark | 31-12-1980  | alice.aardvark@example.com |
Bob Bannerman  | 31-12-1980  |                            | 07700 123654

*import/courses.csv* :-

name         | description
-------------|-----------------------------------------------------
HGV          | Holds a Heavy Goods Vehicle Licence
Fire Marshal | Has undertaken company training to be a fire marshal

*import/peoplecourses.csv* :-

name           | dateOfBirth | course       | gained     | expires
---------------|-------------|--------------|------------|-----------
Alice Aardvark | 31-12-1980  | HGV          | 01-01-2010 | 01-01-2017
Alice Aardvark | 31-12-1980  | Fire Marshal | 01-01-2010 | 01-01-2016
Bob Bannerman  | 31-12-1980  | Fire Marshal | 01-01-2010 | 01-01-2019

## Exporting data

You can export the current database data with

```bash
$ java -jar target/TrainingDatabase-1.0.3-SNAPSHOT.jar export config/local.yml
```

which should drop the data out to the files specified in the configuration in the above format.


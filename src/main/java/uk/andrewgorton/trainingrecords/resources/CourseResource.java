package uk.andrewgorton.trainingrecords.resources;

import com.codahale.metrics.annotation.Timed;
import org.apache.commons.lang3.StringUtils;
import uk.andrewgorton.trainingrecords.TrainingRecordsConfiguration;
import uk.andrewgorton.trainingrecords.api.Course;
import uk.andrewgorton.trainingrecords.api.CourseTrainingRecord;
import uk.andrewgorton.trainingrecords.api.CourseWithTrainingRecords;
import uk.andrewgorton.trainingrecords.core.CourseRefGenerator;
import uk.andrewgorton.trainingrecords.domain.DbWrapper;
import uk.andrewgorton.trainingrecords.views.*;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.List;

@Path("/course")
@Produces(MediaType.APPLICATION_JSON)
public class CourseResource {
    private TrainingRecordsConfiguration configuration;

    public CourseResource(TrainingRecordsConfiguration configuration) {
        this.configuration = configuration;
    }

    @GET
    @Timed
    @Produces(MediaType.TEXT_HTML)
    public CoursesView getCourses() {
        return new CoursesView(new DbWrapper().getCourses());
    }

    @GET
    @Path("/{id}")
    @Timed
    @Produces(MediaType.TEXT_HTML)
    public CourseView getCourse(@PathParam("id") String id) {
        DbWrapper db = new DbWrapper();
        Course c = db.getCourse(id);
        List<CourseTrainingRecord> trainingRecords = db.getCourseTrainingRecords(id);
        CourseWithTrainingRecords cwtr = new CourseWithTrainingRecords();
        cwtr.setCourse(c);
        cwtr.setCourseTrainingRecords(trainingRecords);
        return new CourseView(cwtr);
    }

    @GET
    @Path("/{id}/edit")
    @Timed
    @Produces(MediaType.TEXT_HTML)
    public CourseEditView getCourseEdit(@PathParam("id") String id) {
        return new CourseEditView(new DbWrapper().getCourse(id));
    }

    @GET
    @Path("/{id}/delete")
    @Timed
    @Produces(MediaType.TEXT_HTML)
    public Response doCourseDelete(@PathParam("id") String id) {
        new DbWrapper().deleteCourse(id);
        URI uri = UriBuilder.fromUri("/course").build();
        return Response.seeOther(uri).build();
    }

    @POST
    @Path("/{id}/update")
    @Timed
    @Produces(MediaType.TEXT_HTML)
    public Response doCourseUpdate(@Context HttpServletRequest request,
                                   @PathParam("id") String id,
                                   @FormParam("name") String name,
                                   @FormParam("description") String description) {
        if (StringUtils.isBlank(name)) {
            throw new RuntimeException("Course name cannot be blank");
        }

        // Ensure we don't collide with an existing course
        // (if we're updating ourselves, it's okay though)
        String newRef = new CourseRefGenerator().generateRef(name);
        DbWrapper db = new DbWrapper();
        if (newRef.compareTo(id) != 0 && db.getPerson(newRef) != null) {
            throw new RuntimeException("Can't rename as course exists");
        }
        db.updateCourse(id, name, description);

        URI uri = UriBuilder.fromUri(String.format("/course/%s", newRef)).build();
        return Response.seeOther(uri).build();
    }

    @GET
    @Path("/add")
    @Timed
    @Produces(MediaType.TEXT_HTML)
    public CourseAddView getCourseAdd() {
        return new CourseAddView();
    }

    @POST
    @Path("/add")
    @Timed
    @Produces(MediaType.TEXT_HTML)
    public Response doCourseAdd(@Context HttpServletRequest request,
                                @FormParam("name") String name,
                                @FormParam("description") String description) {
        if (StringUtils.isBlank(name)) {
            throw new RuntimeException("Course name cannot be blank");
        }

        // Ensure we don't collide with an existing course
        String newCourseRef = new CourseRefGenerator().generateRef(name);
        DbWrapper db = new DbWrapper();
        if (db.getCourse(newCourseRef) != null) {
            throw new RuntimeException("Can't rename as course exists");
        }
        db.createCourse(name, description);
        URI uri = UriBuilder.fromUri(String.format("/course/%s", newCourseRef)).build();
        return Response.seeOther(uri).build();
    }
}

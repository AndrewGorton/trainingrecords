package uk.andrewgorton.trainingrecords.resources;

import com.codahale.metrics.annotation.Timed;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import uk.andrewgorton.trainingrecords.TrainingRecordsConfiguration;
import uk.andrewgorton.trainingrecords.api.*;
import uk.andrewgorton.trainingrecords.core.CourseRefGenerator;
import uk.andrewgorton.trainingrecords.core.PersonRefGenerator;
import uk.andrewgorton.trainingrecords.domain.DbWrapper;
import uk.andrewgorton.trainingrecords.views.*;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@Path("/person")
@Produces(MediaType.APPLICATION_JSON)
public class PersonResource {
    private TrainingRecordsConfiguration configuration;

    public PersonResource(TrainingRecordsConfiguration configuration) {
        this.configuration = configuration;
    }

    @GET
    @Timed
    @Produces(MediaType.TEXT_HTML)
    public PersonsView getPersons() {
        return new PersonsView(new DbWrapper().getPersons());
    }

    @GET
    @Path("/{id}")
    @Timed
    @Produces(MediaType.TEXT_HTML)
    public PersonView getPerson(@PathParam("id") String id) {
        DbWrapper db = new DbWrapper();
        Person p = db.getPerson(id);
        List<PersonTrainingRecord> records = db.getPersonTrainingRecords(id);

        PersonWithTrainingRecords ptr = new PersonWithTrainingRecords();
        ptr.setPerson(p);
        ptr.setPersonTrainingRecords(records);


        return new PersonView(ptr);
    }

    @GET
    @Path("/{id}/edit")
    @Timed
    @Produces(MediaType.TEXT_HTML)
    public PersonEditView getPersonEdit(@PathParam("id") String id) {
        return new PersonEditView(new DbWrapper().getPerson(id));
    }

    @GET
    @Path("/{id}/delete")
    @Timed
    @Produces(MediaType.TEXT_HTML)
    public Response doPersonDelete(@PathParam("id") String id) {
        new DbWrapper().deletePerson(id);
        URI uri = UriBuilder.fromUri("/person").build();
        return Response.seeOther(uri).build();
    }
    
    @POST
    @Path("/{id}/update")
    @Timed
    @Produces(MediaType.TEXT_HTML)
    public Response doPersonUpdate(@Context HttpServletRequest request,
                                   @PathParam("id") String id,
                                   @FormParam("name") String name,
                                   @FormParam("dateOfBirth") String dateOfBirth,
                                   @FormParam("email") String email,
                                   @FormParam("mobile") String mobile) {
        if (StringUtils.isBlank(name) || StringUtils.isBlank(dateOfBirth)) {
            throw new RuntimeException("Name or date of birth cannot be blank");
        }

        org.joda.time.format.DateTimeFormatter inputFormat = DateTimeFormat.forPattern(configuration.getImportDateFormat());
        DateTime newDateOfBirth = DateTime.parse(dateOfBirth, inputFormat);

        // Ensure we don't collide with an existing user 
        // (if we're updating ourselves, it's okay though)
        String newPersonRef = new PersonRefGenerator().generateRef(name, newDateOfBirth);
        DbWrapper db = new DbWrapper();
        if (newPersonRef.compareTo(id) != 0 && db.getPerson(newPersonRef) != null) {
            throw new RuntimeException("Can't rename as person exists");
        }
        db.updatePerson(id, name, newDateOfBirth, email, mobile);

        URI uri = UriBuilder.fromUri(String.format("/person/%s", newPersonRef)).build();
        return Response.seeOther(uri).build();
    }

    @GET
    @Path("/add")
    @Timed
    @Produces(MediaType.TEXT_HTML)
    public PersonAddView getPersonAdd() {
        return new PersonAddView();
    }

    @POST
    @Path("/add")
    @Timed
    @Produces(MediaType.TEXT_HTML)
    public Response doPersonAdd(@Context HttpServletRequest request,
                                @FormParam("name") String name,
                                @FormParam("dateOfBirth") String dateOfBirth,
                                @FormParam("email") String email,
                                @FormParam("mobile") String mobile) {

        if (StringUtils.isBlank(name) || StringUtils.isBlank(dateOfBirth)) {
            throw new RuntimeException("Name or date of birth cannot be blank");
        }

        org.joda.time.format.DateTimeFormatter inputFormat = DateTimeFormat.forPattern(configuration.getImportDateFormat());
        DateTime newDateOfBirth = DateTime.parse(dateOfBirth, inputFormat);

        // Ensure we don't collide with an existing user
        // (if we're updating ourselves, it's okay though)
        String newPersonRef = new PersonRefGenerator().generateRef(name, newDateOfBirth);
        DbWrapper db = new DbWrapper();
        if (db.getPerson(newPersonRef) != null) {
            throw new RuntimeException("Can't rename as person exists");
        }
        db.createPerson(name, newDateOfBirth, email, mobile);
        URI uri = UriBuilder.fromUri(String.format("/person/%s", newPersonRef)).build();
        return Response.seeOther(uri).build();
    }

    @GET
    @Path("/{id}/editTrainingRecord")
    @Timed
    @Produces(MediaType.TEXT_HTML)
    public PersonEditTrainingRecordView getAddTrainingRecord(@PathParam("id") String id) {
        DbWrapper db = new DbWrapper();
        Person p = db.getPerson(id);
        List<PersonTrainingRecord> personTrainingRecords = db.getPersonTrainingRecords(id);
        List<Course> courses = db.getCourses();

        // Mangle courses and training records together
        List<AllCoursesPersonTrainingRecord> allCoursesAndTrainingRecords = new ArrayList<AllCoursesPersonTrainingRecord>();
        for (Course singleCourse : courses) {
            // Does the user have the course?
            boolean userHasTrainingInThis = false;
            for (PersonTrainingRecord singleTrainingRecord : personTrainingRecords) {
                if (singleTrainingRecord.getCourseRef().compareTo(singleCourse.getCourseRef()) == 0) {

                    AllCoursesPersonTrainingRecord tempTr = new AllCoursesPersonTrainingRecord();
                    tempTr.setCourseRef(singleTrainingRecord.getCourseRef());
                    tempTr.setCourseTitle(singleTrainingRecord.getCourseTitle());
                    tempTr.setGained(singleTrainingRecord.getGained());
                    tempTr.setExpires(singleTrainingRecord.getExpires());
                    allCoursesAndTrainingRecords.add(tempTr);

                    userHasTrainingInThis = true;
                    break;
                }
            }
            if (!userHasTrainingInThis) {
                AllCoursesPersonTrainingRecord tempTr = new AllCoursesPersonTrainingRecord();
                tempTr.setCourseRef(singleCourse.getCourseRef());
                tempTr.setCourseTitle(singleCourse.getName());
                allCoursesAndTrainingRecords.add(tempTr);
            }
        }

        return new PersonEditTrainingRecordView(p, allCoursesAndTrainingRecords);
    }

    @POST
    @Path("/{id}/updateTrainingRecord")
    @Timed
    @Produces(MediaType.TEXT_HTML)
    public Response updateTrainingRecord(@PathParam("id") String id, MultivaluedMap<String, String> formParams) {
        for (MultivaluedMap.Entry<String, List<String>> entry : formParams.entrySet()) {
            System.out.println(entry.getKey());
            for (String singleEntry : entry.getValue()) {
                System.out.println("  " + singleEntry);
            }
        }

        CourseRefGenerator crg = new CourseRefGenerator();
        List<PersonTrainingRecord> ptrs = new ArrayList<PersonTrainingRecord>();
        for (MultivaluedMap.Entry<String, List<String>> entry : formParams.entrySet()) {
            if (entry.getKey().endsWith("-expires") || entry.getKey().endsWith("-gained")) {
                String courseName;
                boolean isGained = false;

                // Process it
                if (entry.getKey().endsWith("-expires")) {
                    courseName = entry.getKey().substring(0, entry.getKey().length() - "-expires".length());
                    isGained = false;
                } else {
                    courseName = entry.getKey().substring(0, entry.getKey().length() - "-gained".length());
                    isGained = true;
                }

                if (isGained) {
                    System.out.println("Gained course: " + courseName);
                } else {
                    System.out.println("Course expires: " + courseName);
                }
                String courseRef = crg.generateRef(courseName);

                if (entry.getValue().size() == 1) {
                    String date = entry.getValue().get(0);

                    PersonTrainingRecord matchedRecord = null;
                    for (PersonTrainingRecord singleRecord : ptrs) {
                        if (singleRecord.getCourseRef().compareTo(courseRef) == 0) {
                            matchedRecord = singleRecord;
                            break;
                        }
                    }

                    if (matchedRecord == null) {
                        matchedRecord = new PersonTrainingRecord();
                        matchedRecord.setCourseRef(courseRef);
                        matchedRecord.setCourseTitle(courseName);
                        if (StringUtils.isNotBlank(date)) {
                            if (isGained) {
                                matchedRecord.setGained(DateTime.parse(date, DateTimeFormat.forPattern("dd-MM-yyyy")));
                            } else {
                                matchedRecord.setExpires(DateTime.parse(date, DateTimeFormat.forPattern("dd-MM-yyyy")));
                            }
                            ptrs.add(matchedRecord);
                        }
                    } else {
                        if (StringUtils.isNotBlank(date)) {
                            if (isGained) {
                                matchedRecord.setGained(DateTime.parse(date, DateTimeFormat.forPattern("dd-MM-yyyy")));
                            } else {
                                matchedRecord.setExpires(DateTime.parse(date, DateTimeFormat.forPattern("dd-MM-yyyy")));
                            }
                        }
                    }
                }
            }
        }
        
        new DbWrapper().updatePersonTrainingRecords(id, ptrs);


        URI uri = UriBuilder.fromUri(String.format("/person/%s", id)).build();
        return Response.seeOther(uri).build();
    }
}

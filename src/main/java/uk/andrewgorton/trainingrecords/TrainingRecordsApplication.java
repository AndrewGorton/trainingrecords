package uk.andrewgorton.trainingrecords;

import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.views.ViewBundle;
import uk.andrewgorton.trainingrecords.cli.Export;
import uk.andrewgorton.trainingrecords.cli.Import;
import uk.andrewgorton.trainingrecords.cli.InteractiveQuery;
import uk.andrewgorton.trainingrecords.domain.DbWrapper;
import uk.andrewgorton.trainingrecords.resources.CourseResource;
import uk.andrewgorton.trainingrecords.resources.PersonResource;

public class TrainingRecordsApplication extends Application<TrainingRecordsConfiguration> {
    public static void main(String[] args) throws Exception {
        new TrainingRecordsApplication().run(args);
    }

    @Override
    public void initialize(Bootstrap<TrainingRecordsConfiguration> bootstrap) {
        bootstrap.addCommand(new Import());
        bootstrap.addCommand(new Export());
        bootstrap.addCommand(new InteractiveQuery());

        bootstrap.addBundle(new AssetsBundle("/assets/", "/assets"));
        bootstrap.addBundle(new ViewBundle());
    }

    @Override
    public void run(TrainingRecordsConfiguration configuration,
                    Environment environment) {
        new DbWrapper().initialise(configuration.getDbpath());

        environment.jersey().register(new PersonResource(configuration));
        environment.jersey().register(new CourseResource(configuration));
    }
}

package uk.andrewgorton.trainingrecords.api;

import java.util.List;

public class PersonWithTrainingRecords  {
    private Person person;
    private List<PersonTrainingRecord> personTrainingRecords = null;

    public PersonWithTrainingRecords() {

    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public List<PersonTrainingRecord> getPersonTrainingRecords() {
        return personTrainingRecords;
    }

    public void setPersonTrainingRecords(List<PersonTrainingRecord> personTrainingRecords) {
        this.personTrainingRecords = personTrainingRecords;
    }
}

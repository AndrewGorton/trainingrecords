package uk.andrewgorton.trainingrecords.api;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

public class AllCoursesPersonTrainingRecord {
    private String courseTitle;
    private String courseRef;
    private DateTime gained;
    private DateTime expires;

    private static final String dateFormatHumanReadable = "dd-MM-yyyy";

    public AllCoursesPersonTrainingRecord() {

    }

    public String getCourseTitle() {
        return courseTitle;
    }

    public void setCourseTitle(String courseTitle) {
        this.courseTitle = courseTitle;
    }

    public DateTime getGained() {
        return gained;
    }

    public String getGainedHumanReadable() {
        if (gained != null) {
            return DateTimeFormat.forPattern(dateFormatHumanReadable).print(gained);
        }
        return "";

    }

    public void setGained(DateTime gained) {
        this.gained = gained;
    }

    public DateTime getExpires() {
        return expires;
    }

    public String getExpiresHumanReadable() {
        if (expires != null) {
            return DateTimeFormat.forPattern(dateFormatHumanReadable).print(expires);
        }
        return "";
    }

    public void setExpires(DateTime expires) {
        this.expires = expires;
    }

    public String getCourseRef() {
        return courseRef;
    }

    public void setCourseRef(String courseRef) {
        this.courseRef = courseRef;
    }
}

package uk.andrewgorton.trainingrecords.api;

import java.util.List;

public class CourseWithTrainingRecords {
    private Course course;
    private List<CourseTrainingRecord> courseTrainingRecords;

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public List<CourseTrainingRecord> getCourseTrainingRecords() {
        return courseTrainingRecords;
    }

    public void setCourseTrainingRecords(List<CourseTrainingRecord> courseTrainingRecords) {
        this.courseTrainingRecords = courseTrainingRecords;
    }
}

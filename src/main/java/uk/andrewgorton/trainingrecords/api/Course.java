package uk.andrewgorton.trainingrecords.api;

import org.apache.commons.lang3.StringUtils;

public class Course {
    private String name;
    private String description;
    private String courseRef;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCourseRef() {
        return courseRef;
    }

    public void setCourseRef(String courseRef) {
        this.courseRef = courseRef;
    }

    // Generated fields below
    public boolean hasDescription() {
        if (StringUtils.isNotBlank(description)) {
            return true;
        }
        return false;
    }

    public boolean hasNoDescription() {
        return !hasDescription();
    }
}


package uk.andrewgorton.trainingrecords.api;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

public class CourseTrainingRecord {
    private String personName;
    private String personRef;
    
    private DateTime gained;
    private DateTime expires;
    
    private String courseRef;
    
    private static final String dateFormatHumanReadable = "dd-MM-yyyy";

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getPersonRef() {
        return personRef;
    }

    public void setPersonRef(String personRef) {
        this.personRef = personRef;
    }

    public DateTime getGained() {
        return gained;
    }

    public void setGained(DateTime gained) {
        this.gained = gained;
    }

    public DateTime getExpires() {
        return expires;
    }

    public void setExpires(DateTime expires) {
        this.expires = expires;
    }

    public String getCourseRef() {
        return courseRef;
    }

    public void setCourseRef(String courseRef) {
        this.courseRef = courseRef;
    }

    // Generated fields
    public String getGainedHumanReadable() {
        return DateTimeFormat.forPattern(dateFormatHumanReadable).print(gained);
    }

    public String getExpiresHumanReadable() {
        return DateTimeFormat.forPattern(dateFormatHumanReadable).print(expires);
    }
}

package uk.andrewgorton.trainingrecords.views;

import io.dropwizard.views.View;
import uk.andrewgorton.trainingrecords.api.AllCoursesPersonTrainingRecord;
import uk.andrewgorton.trainingrecords.api.Person;

import java.util.List;

public class PersonEditTrainingRecordView extends View {
    private final Person person;
    private List<AllCoursesPersonTrainingRecord> personTrainingRecords;

    public PersonEditTrainingRecordView(Person person, List<AllCoursesPersonTrainingRecord> personTrainingRecords) {
        super("personedittraining.mustache");
        this.person = person;
        this.personTrainingRecords = personTrainingRecords;
    }

    public Person getPerson() {
        return person;
    }

    public List<AllCoursesPersonTrainingRecord> getAllCoursesPersonTrainingRecords() {
        return personTrainingRecords;
    }
}

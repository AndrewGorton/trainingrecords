package uk.andrewgorton.trainingrecords.views;

import io.dropwizard.views.View;
import uk.andrewgorton.trainingrecords.api.Course;

public class CourseEditView extends View {
    private final Course course;

    public CourseEditView(Course course) {
        super("courseedit.mustache");
        this.course = course;
    }

    public Course getCourse() {
        return course;
    }
}

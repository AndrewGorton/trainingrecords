package uk.andrewgorton.trainingrecords.views;

import io.dropwizard.views.View;
import uk.andrewgorton.trainingrecords.api.Course;

import java.util.List;

public class CoursesView extends View {
    private final List<Course> courses;

    public CoursesView(List<Course> courses) {
        super("courses.mustache");
        this.courses = courses;
    }

    public List<Course> getCourses() {
        return courses;
    }
}

package uk.andrewgorton.trainingrecords.views;

import io.dropwizard.views.View;
import uk.andrewgorton.trainingrecords.api.PersonWithTrainingRecords;

public class PersonView extends View {
    private final PersonWithTrainingRecords personWithTrainingRecords;

    public PersonView(PersonWithTrainingRecords personWithTrainingRecords) {
        super("person.mustache");
        this.personWithTrainingRecords = personWithTrainingRecords;
    }

    public PersonWithTrainingRecords getPersonWithTrainingRecords() {
        return personWithTrainingRecords;
    }
}

package uk.andrewgorton.trainingrecords.views;

import io.dropwizard.views.View;

public class CourseAddView extends View {
    public CourseAddView() {
        super("courseadd.mustache");
    }
}

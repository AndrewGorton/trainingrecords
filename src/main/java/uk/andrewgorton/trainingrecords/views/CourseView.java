package uk.andrewgorton.trainingrecords.views;

import io.dropwizard.views.View;
import uk.andrewgorton.trainingrecords.api.CourseWithTrainingRecords;

public class CourseView extends View {
    private final CourseWithTrainingRecords courseWithTrainingRecords;

    public CourseView(CourseWithTrainingRecords courseWithTrainingRecords) {
        super("course.mustache");
        this.courseWithTrainingRecords = courseWithTrainingRecords;
    }

    public CourseWithTrainingRecords getCourseWithTrainingRecords() {
        return courseWithTrainingRecords;
    }
}

package uk.andrewgorton.trainingrecords.views;

import io.dropwizard.views.View;
import uk.andrewgorton.trainingrecords.api.Person;

import java.util.List;

public class PersonsView extends View {
    private final List<Person> persons;

    public PersonsView(List<Person> persons) {
        super("persons.mustache");
        this.persons = persons;
    }

    public List<Person> getPersons() {
        return persons;
    }
}

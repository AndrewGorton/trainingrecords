package uk.andrewgorton.trainingrecords.views;

import io.dropwizard.views.View;

public class PersonAddView extends View {
    public PersonAddView() {
        super("personadd.mustache");
    }
}

package uk.andrewgorton.trainingrecords.views;

import io.dropwizard.views.View;
import uk.andrewgorton.trainingrecords.api.Person;

public class PersonEditView extends View {
    private final Person person;

    public PersonEditView(Person person) {
        super("personedit.mustache");
        this.person = person;
    }

    public Person getPerson() {
        return person;
    }
}

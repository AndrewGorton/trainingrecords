package uk.andrewgorton.trainingrecords;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TrainingRecordsConfiguration extends Configuration {
    private String dbpath;

    private String importDateFormat;
    private String importCoursesCsv;
    private String importPeopleCsv;
    private String importPeopleCoursesCsv;

    private String exportDateFormat;
    private String exportCoursesCsv;
    private String exportPeopleCsv;
    private String exportPeopleCoursesCsv;

    @JsonProperty
    public String getDbpath() {
        return dbpath;
    }

    @JsonProperty
    public void setDbpath(String dbpath) {
        this.dbpath = dbpath;
    }

    @JsonProperty
    public String getImportCoursesCsv() {
        return importCoursesCsv;
    }

    @JsonProperty
    public void setImportCoursesCsv(String importCoursesCsv) {
        this.importCoursesCsv = importCoursesCsv;
    }

    @JsonProperty
    public String getImportPeopleCsv() {
        return importPeopleCsv;
    }

    @JsonProperty
    public void setImportPeopleCsv(String importPeopleCsv) {
        this.importPeopleCsv = importPeopleCsv;
    }

    @JsonProperty
    public String getImportDateFormat() {
        return importDateFormat;
    }

    @JsonProperty
    public void setImportDateFormat(String importDateFormat) {
        this.importDateFormat = importDateFormat;
    }

    @JsonProperty
    public String getImportPeopleCoursesCsv() {
        return importPeopleCoursesCsv;
    }

    @JsonProperty
    public void setImportPeopleCoursesCsv(String importPeopleCoursesCsv) {
        this.importPeopleCoursesCsv = importPeopleCoursesCsv;
    }

    @JsonProperty
    public String getExportDateFormat() {
        return exportDateFormat;
    }

    @JsonProperty
    public void setExportDateFormat(String exportDateFormat) {
        this.exportDateFormat = exportDateFormat;
    }

    @JsonProperty
    public String getExportCoursesCsv() {
        return exportCoursesCsv;
    }

    @JsonProperty
    public void setExportCoursesCsv(String exportCoursesCsv) {
        this.exportCoursesCsv = exportCoursesCsv;
    }

    @JsonProperty
    public String getExportPeopleCsv() {
        return exportPeopleCsv;
    }

    @JsonProperty
    public void setExportPeopleCsv(String exportPeopleCsv) {
        this.exportPeopleCsv = exportPeopleCsv;
    }

    @JsonProperty
    public String getExportPeopleCoursesCsv() {
        return exportPeopleCoursesCsv;
    }

    @JsonProperty
    public void setExportPeopleCoursesCsv(String exportPeopleCoursesCsv) {
        this.exportPeopleCoursesCsv = exportPeopleCoursesCsv;
    }
}

package uk.andrewgorton.trainingrecords.cli;

import io.dropwizard.cli.ConfiguredCommand;
import io.dropwizard.setup.Bootstrap;
import net.sourceforge.argparse4j.inf.Namespace;
import org.apache.commons.codec.binary.Base32;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.neo4j.graphdb.*;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import uk.andrewgorton.trainingrecords.TrainingRecordsConfiguration;
import uk.andrewgorton.trainingrecords.core.GraphDbShutdownHooker;
import uk.andrewgorton.trainingrecords.core.PersonRefGenerator;
import uk.andrewgorton.trainingrecords.domain.NodeLabel;
import uk.andrewgorton.trainingrecords.domain.RelType;

import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

// Exports out to CSV
public class Export extends ConfiguredCommand<TrainingRecordsConfiguration> {
    public Export() {
        super("export", "Exports data to a set of files");
    }

    @Override
    protected void run(Bootstrap<TrainingRecordsConfiguration> bootstrap, Namespace namespace, TrainingRecordsConfiguration trainingRecordsConfiguration) throws Exception {
        if (StringUtils.isBlank(trainingRecordsConfiguration.getDbpath())) {
            throw new RuntimeException("No database path specified in configuration");
        }
        if (StringUtils.isBlank(trainingRecordsConfiguration.getImportCoursesCsv())) {
            throw new RuntimeException("Import location for courses not specified in configuration");
        }

        System.out.println("Using database in: " + trainingRecordsConfiguration.getDbpath());
        GraphDatabaseService graphDb = null;
        try {
            graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(trainingRecordsConfiguration.getDbpath());
            GraphDbShutdownHooker.registerShutdownHook(graphDb);

            exportPeopleToFile(graphDb, trainingRecordsConfiguration.getExportDateFormat(), trainingRecordsConfiguration.getExportPeopleCsv());
            exportCoursesToFile(graphDb, trainingRecordsConfiguration.getExportCoursesCsv());
            exportPeopleAndCoursesToFile(graphDb, trainingRecordsConfiguration.getExportDateFormat(), trainingRecordsConfiguration.getExportPeopleCoursesCsv());
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (graphDb != null) {
            graphDb.shutdown();
        }
    }


    private void exportPeopleToFile(final GraphDatabaseService graphDb, final String dateFormat, final String filename) {
        System.out.println("Exporting people");

        Base32 encoder = new Base32();
        DateTimeFormatter outputFormat = DateTimeFormat.forPattern(dateFormat);

        try {
            FileWriter out = new FileWriter(filename);
            CSVFormat csvFileFormat = CSVFormat.EXCEL.withHeader("name", "dateOfBirth", "email", "mobile");
            CSVPrinter csvFilePrinter = new CSVPrinter(out, csvFileFormat);

            try (Transaction ignored = graphDb.beginTx()) {
                ResourceIterator<Node> ri = graphDb.findNodes(NodeLabel.PERSON);
                while (ri.hasNext()) {
                    Node currentNode = ri.next();
                    List<Object> record = new ArrayList<Object>();
                    record.add((String) currentNode.getProperty("name"));
                    DateTime dateOfBirth = new DateTime((long) currentNode.getProperty("dateOfBirth"));
                    record.add(outputFormat.print(dateOfBirth));
                    if (currentNode.hasProperty("email")) {
                        record.add((String) currentNode.getProperty("email"));
                    } else {
                        record.add("");
                    }
                    if (currentNode.hasProperty("mobile")) {
                        record.add((String) currentNode.getProperty("mobile"));
                    } else {
                        record.add("");
                    }
                    csvFilePrinter.printRecord(record);
                }
            }

            csvFilePrinter.close();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void exportCoursesToFile(final GraphDatabaseService graphDb, final String filename) {
        System.out.println("Exporting courses");

        try {
            FileWriter out = new FileWriter(filename);
            CSVFormat csvFileFormat = CSVFormat.EXCEL.withHeader("name", "description");
            CSVPrinter csvFilePrinter = new CSVPrinter(out, csvFileFormat);

            try (Transaction ignored = graphDb.beginTx()) {
                ResourceIterator<Node> ri = graphDb.findNodes(NodeLabel.COURSE);
                while (ri.hasNext()) {
                    Node currentNode = ri.next();
                    List<Object> record = new ArrayList<Object>();
                    record.add((String) currentNode.getProperty("name"));
                    if (currentNode.hasProperty("description")) {
                        record.add((String) currentNode.getProperty("description"));
                    } else {
                        record.add("");
                    }
                    csvFilePrinter.printRecord(record);
                }
            }

            csvFilePrinter.close();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void exportPeopleAndCoursesToFile(final GraphDatabaseService graphDb, final String dateFormat, final String filename) {
        System.out.println("Exporting people to courses");

        try {
            FileWriter out = new FileWriter(filename);
            CSVFormat csvFileFormat = CSVFormat.EXCEL.withHeader("name", "dateOfBirth", "course", "gained", "expires");
            CSVPrinter csvFilePrinter = new CSVPrinter(out, csvFileFormat);
            DateTimeFormatter outputFormat = DateTimeFormat.forPattern(dateFormat);

            try (Transaction ignored = graphDb.beginTx()) {
                ResourceIterator<Node> ri = graphDb.findNodes(NodeLabel.PERSON);
                while (ri.hasNext()) {
                    Node currentNode = ri.next();
                    for (Relationship r : currentNode.getRelationships(RelType.HAS)) {
                        List<Object> record = new ArrayList<Object>();
                        record.add(currentNode.getProperty("name"));
                        record.add(outputFormat.print(new DateTime(currentNode.getProperty("dateOfBirth"))));
                        Node course = r.getEndNode();
                        record.add(course.getProperty("name"));
                        record.add(outputFormat.print(new DateTime(r.getProperty("gained"))));
                        record.add(outputFormat.print(new DateTime(r.getProperty("expires"))));
                        csvFilePrinter.printRecord(record);
                    }
                }
            }

            csvFilePrinter.close();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

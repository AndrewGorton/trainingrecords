package uk.andrewgorton.trainingrecords.cli;

import io.dropwizard.cli.ConfiguredCommand;
import io.dropwizard.setup.Bootstrap;
import net.sourceforge.argparse4j.inf.Namespace;
import org.apache.commons.lang3.StringUtils;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Result;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import uk.andrewgorton.trainingrecords.TrainingRecordsConfiguration;
import uk.andrewgorton.trainingrecords.core.GraphDbShutdownHooker;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;

public class InteractiveQuery extends ConfiguredCommand<TrainingRecordsConfiguration> {
    public InteractiveQuery() {
        super("query", "Queries the DB");
    }

    @Override
    protected void run(Bootstrap<TrainingRecordsConfiguration> bootstrap, Namespace namespace, TrainingRecordsConfiguration trainingRecordsConfiguration) throws Exception {
        if (StringUtils.isBlank(trainingRecordsConfiguration.getDbpath())) {
            throw new RuntimeException("No database path specified in configuration");
        }

        System.out.println("Using database in: " + trainingRecordsConfiguration.getDbpath());
        GraphDatabaseService graphDb = null;
        try {
            graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(trainingRecordsConfiguration.getDbpath());
            GraphDbShutdownHooker.registerShutdownHook(graphDb);

            while (true) {
                System.out.print("Enter your query: ");
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                try {
                    String line = br.readLine();
                    if (line.compareToIgnoreCase("exit") == 0) {
                        break;
                    }
                    queryDatabase(graphDb, line);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        // Close any outstanding resources
        if (graphDb != null) {
            graphDb.shutdown();
        }
    }

    private void queryDatabase(final GraphDatabaseService graphDb, final String query) {
        try (Transaction ignored = graphDb.beginTx();
             Result result = graphDb.execute(query)) {
            while (result.hasNext()) {
                Map<String, Object> row = result.next();
                for (Map.Entry<String, Object> column : row.entrySet()) {
                    System.out.println(column.getKey() + ": " + column.getValue() + "; ");
                }
            }
        }

    }
}

package uk.andrewgorton.trainingrecords.cli;

import io.dropwizard.cli.ConfiguredCommand;
import io.dropwizard.setup.Bootstrap;
import net.sourceforge.argparse4j.inf.Namespace;
import org.apache.commons.codec.binary.Base32;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import uk.andrewgorton.trainingrecords.TrainingRecordsConfiguration;
import uk.andrewgorton.trainingrecords.core.CourseRefGenerator;
import uk.andrewgorton.trainingrecords.core.GraphDbShutdownHooker;
import uk.andrewgorton.trainingrecords.core.PersonRefGenerator;
import uk.andrewgorton.trainingrecords.domain.NodeLabel;
import uk.andrewgorton.trainingrecords.domain.RelType;

import java.io.File;
import java.io.FileReader;
import java.security.MessageDigest;

// Wipes out the database and reinserts data from the supplied files
public class Import extends ConfiguredCommand<TrainingRecordsConfiguration> {
    public Import() {
        super("import", "Wipes the database and imports it from a file");
    }

    @Override
    protected void run(Bootstrap<TrainingRecordsConfiguration> bootstrap, Namespace namespace, TrainingRecordsConfiguration trainingRecordsConfiguration) throws Exception {
        if (StringUtils.isBlank(trainingRecordsConfiguration.getDbpath())) {
            throw new RuntimeException("No database path specified in configuration");
        }
        if (StringUtils.isBlank(trainingRecordsConfiguration.getImportCoursesCsv())) {
            throw new RuntimeException("Import location for courses not specified in configuration");
        }

        // Delete the path if it exists
        File f = new File(trainingRecordsConfiguration.getDbpath());
        if (f.isDirectory()) {
            System.out.println("Wiping directory: " + trainingRecordsConfiguration.getDbpath());
            FileUtils.deleteDirectory(f);
        }

        System.out.println("Creating directory in: " + trainingRecordsConfiguration.getDbpath());
        GraphDatabaseService graphDb = null;
        try {
            graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(trainingRecordsConfiguration.getDbpath());
            GraphDbShutdownHooker.registerShutdownHook(graphDb);

            importCoursesFromFile(graphDb, trainingRecordsConfiguration.getImportCoursesCsv());
            importPeopleFromFile(graphDb, trainingRecordsConfiguration.getImportDateFormat(), trainingRecordsConfiguration.getImportPeopleCsv());
            importPeopleCoursesFromFile(graphDb, trainingRecordsConfiguration.getImportDateFormat(), trainingRecordsConfiguration.getImportPeopleCoursesCsv());
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Close any outstanding resources
        if (graphDb != null) {
            graphDb.shutdown();
        }
    }

    private void importCoursesFromFile(final GraphDatabaseService graphDb, final String filename) {
        System.out.println("Importing courses");

        try {
            // Enforce unique course names
            try (Transaction tx = graphDb.beginTx()) {
                graphDb.schema().constraintFor(NodeLabel.COURSE).assertPropertyIsUnique("courseRef").create();
                tx.success();
            }

            FileReader in = new FileReader(filename);
            Iterable<CSVRecord> records = CSVFormat.EXCEL.withHeader().parse(in);
            for (CSVRecord record : records) {
                final String courseName = record.get("name");
                final String courseDescription = record.get("description");
                final String courseReference = new CourseRefGenerator().generateRef(courseName);

                try (Transaction tx = graphDb.beginTx()) {
                    Node courseNode = graphDb.createNode();
                    courseNode.addLabel(NodeLabel.COURSE);
                    courseNode.setProperty("courseRef", courseReference);
                    courseNode.setProperty("name", courseName);
                    courseNode.setProperty("description", courseDescription);
                    System.out.println(String.format("Adding course '%s' ('%s' / '%s')", courseName, courseDescription, courseReference));
                    tx.success();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    private void importPeopleFromFile(final GraphDatabaseService graphDb, final String dateFormat, final String filename) {
        System.out.println("Importing people");

        Base32 encoder = new Base32();
        DateTimeFormatter inputFormat = DateTimeFormat.forPattern(dateFormat);

        try {
            // Enforce unique personRefs
            try (Transaction tx = graphDb.beginTx()) {
                graphDb.schema().constraintFor(NodeLabel.PERSON).assertPropertyIsUnique("personRef").create();
                tx.success();
            }

            FileReader in = new FileReader(filename);
            Iterable<CSVRecord> records = CSVFormat.EXCEL.withHeader().parse(in);
            for (CSVRecord record : records) {
                final String personName = record.get("name");
                final DateTime personDateOfBirth = DateTime.parse(record.get("dateOfBirth"), inputFormat);
                final String email = record.get("email");
                final String mobile = record.get("mobile");

                final String personRef = new PersonRefGenerator().generateRef(personName, personDateOfBirth);

                try (Transaction tx = graphDb.beginTx()) {
                    Node personNode = graphDb.createNode();
                    personNode.addLabel(NodeLabel.PERSON);
                    personNode.setProperty("name", personName);
                    personNode.setProperty("dateOfBirth", personDateOfBirth.getMillis());
                    personNode.setProperty("personRef", personRef);
                    personNode.setProperty("email", email);
                    personNode.setProperty("mobile", mobile);

                    System.out.println(String.format("Adding person '%s' ('%s')", personName, personDateOfBirth.getMillis()));
                    tx.success();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    private void importPeopleCoursesFromFile(final GraphDatabaseService graphDb, final String dateFormat, final String filename) {
        System.out.println("Importing people-to-courses");

        DateTimeFormatter inputFormat = DateTimeFormat.forPattern(dateFormat);
        CourseRefGenerator crg = new CourseRefGenerator();
        PersonRefGenerator prg = new PersonRefGenerator();
        try {
            FileReader in = new FileReader(filename);
            Iterable<CSVRecord> records = CSVFormat.EXCEL.withHeader().parse(in);
            for (CSVRecord record : records) {
                final String personName = record.get("name");
                final DateTime personDateOfBirth = DateTime.parse(record.get("dateOfBirth"), inputFormat);

                final String personRef = prg.generateRef(personName, personDateOfBirth);

                final String courseName = record.get("course");
                final DateTime gained = DateTime.parse(record.get("gained"), inputFormat);
                DateTime expires = null;
                if (record.isSet("expires")) {
                    expires = DateTime.parse(record.get("expires"), inputFormat);
                }

                // Find the person
                Node personNode = null;
                try (Transaction ignored = graphDb.beginTx()) {
                    personNode = graphDb.findNode(NodeLabel.PERSON, "personRef", personRef);
                }
                if (personNode == null) {
                    throw new RuntimeException(String.format("Failed to find person '%s' (born '%s')", personName, inputFormat.print(personDateOfBirth)));
                }

                // Get the course
                Node courseNode = null;
                try (Transaction ignored = graphDb.beginTx()) {
                    String courseRef = crg.generateRef(courseName);
                    courseNode = graphDb.findNode(NodeLabel.COURSE, "courseRef", courseRef);
                }
                if (courseNode == null) {
                    throw new RuntimeException(String.format("Failed to find course '%s'", courseName));
                }

                // Add the relationship to the course
                try (Transaction tx = graphDb.beginTx()) {
                    Relationship r = personNode.createRelationshipTo(courseNode, RelType.HAS);
                    r.setProperty("gained", gained.getMillis());
                    if (expires != null) {
                        r.setProperty("expires", expires.getMillis());
                    }

                    System.out.println(String.format("User '%s' has course '%s'", personName, courseNode.getProperty("name")));
                    tx.success();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }
}

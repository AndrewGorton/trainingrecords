package uk.andrewgorton.trainingrecords.core;

import org.apache.commons.codec.binary.Base32;
import org.joda.time.DateTime;

import java.security.MessageDigest;

public class CourseRefGenerator {
    public String generateRef(final String name) {
        try {
            return new Base32().encodeAsString(name.getBytes());

        } catch (Exception e) {
            throw new RuntimeException("Failed to generate reference", e);
        }
    }
}

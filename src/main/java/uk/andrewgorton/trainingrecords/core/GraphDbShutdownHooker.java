package uk.andrewgorton.trainingrecords.core;

import org.neo4j.graphdb.GraphDatabaseService;

public class GraphDbShutdownHooker {

    public static void registerShutdownHook(final GraphDatabaseService graphDb) {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                System.out.println("Shutting down GraphDB");
                graphDb.shutdown();
            }
        });
    }

}

package uk.andrewgorton.trainingrecords.core;

import org.joda.time.DateTime;
import uk.andrewgorton.trainingrecords.api.Person;
import org.neo4j.graphdb.*;
import uk.andrewgorton.trainingrecords.api.PersonWithTrainingRecords;

public class PersonFactory {
    public Person getPerson(Node node) {
        Person tempPerson = new Person();
        tempPerson.setName((String) node.getProperty("name"));
        tempPerson.setDateOfBirth(new DateTime(node.getProperty("dateOfBirth")));
        tempPerson.setPersonRef((String) node.getProperty("personRef"));
        if (node.hasProperty("email")) {
            tempPerson.setEmail((String) node.getProperty("email"));
        }
        if (node.hasProperty("mobile")) {
            tempPerson.setMobile((String) node.getProperty("mobile"));
        }
        return tempPerson;
    }
}

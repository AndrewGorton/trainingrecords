package uk.andrewgorton.trainingrecords.core;

import org.apache.commons.codec.binary.Base32;
import org.joda.time.DateTime;

import java.security.MessageDigest;

public class PersonRefGenerator {
    public String generateRef(final String name, final DateTime dateOfBirth) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            md.reset();
            byte[] buffer = String.format("%s %s", name, dateOfBirth.getMillis()).getBytes();
            md.update(buffer);
            byte[] digest = md.digest();
            return new Base32().encodeAsString(digest);
            
        } catch (Exception e) {
            throw new RuntimeException("Failed to generate reference", e);
        }
    }
}

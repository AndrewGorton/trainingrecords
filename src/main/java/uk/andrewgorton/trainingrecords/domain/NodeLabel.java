package uk.andrewgorton.trainingrecords.domain;

import org.neo4j.graphdb.Label;

public enum NodeLabel implements Label {
    PERSON,
    COURSE
}

package uk.andrewgorton.trainingrecords.domain;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.neo4j.graphdb.*;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import uk.andrewgorton.trainingrecords.api.*;
import uk.andrewgorton.trainingrecords.core.CourseRefGenerator;
import uk.andrewgorton.trainingrecords.core.GraphDbShutdownHooker;
import uk.andrewgorton.trainingrecords.core.PersonFactory;
import uk.andrewgorton.trainingrecords.core.PersonRefGenerator;

import java.util.ArrayList;
import java.util.List;

public class DbWrapper {
    private static GraphDatabaseService db;

    public DbWrapper() {

    }

    public void initialise(final String path) {
        try {
            db = new GraphDatabaseFactory().newEmbeddedDatabase(path);
            GraphDbShutdownHooker.registerShutdownHook(db);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Person> getPersons() {
        List<Person> persons = new ArrayList<Person>();

        try (Transaction ignored = db.beginTx()) {
            ResourceIterator<Node> ri = db.findNodes(NodeLabel.PERSON);
            while (ri.hasNext()) {
                Node currentNode = ri.next();
                persons.add(new PersonFactory().getPerson(currentNode));
            }
        }

        return persons;
    }

    public Person getPerson(String personRef) {
        try (Transaction ignored = db.beginTx()) {
            Node n = db.findNode(NodeLabel.PERSON, "personRef", personRef);
            if (n == null) {
                return null;
            }

            return new PersonFactory().getPerson(n);
        }
    }

    public List<PersonTrainingRecord> getPersonTrainingRecords(final String personRef) {
        List<PersonTrainingRecord> personTrainingRecords = new ArrayList<PersonTrainingRecord>();

        try (Transaction ignored = db.beginTx()) {
            Node person = db.findNode(NodeLabel.PERSON, "personRef", personRef);
            if (person == null) {
                return null;
            }

            for (Relationship singleRelationship : person.getRelationships(RelType.HAS)) {
                Node course = singleRelationship.getEndNode();

                // Filter out expired training
                if (singleRelationship.hasProperty("expires")) {
                    DateTime expires = new DateTime(singleRelationship.getProperty("expires"));
                    if (expires.isBeforeNow()) {
                        continue;
                    }
                }

                PersonTrainingRecord tr = new PersonTrainingRecord();
                tr.setCourseRef((String) course.getProperty("courseRef"));
                tr.setCourseTitle((String) course.getProperty("name"));
                tr.setGained(new DateTime(singleRelationship.getProperty("gained")));
                tr.setExpires(new DateTime(singleRelationship.getProperty("expires")));
                tr.setCourseRef((String) course.getProperty("courseRef"));
                personTrainingRecords.add(tr);
            }

            return personTrainingRecords;
        }
    }

    public void updatePerson(final String currentPersonRef,
                             final String newName,
                             final DateTime dateOfBirth,
                             final String email,
                             final String mobile) {
        try (Transaction tx = db.beginTx()) {
            Node n = db.findNode(NodeLabel.PERSON, "personRef", currentPersonRef);
            if (n == null) {
                throw new RuntimeException("Cannot update a missing user");
            }

            n.setProperty("personRef", new PersonRefGenerator().generateRef(newName, dateOfBirth));
            n.setProperty("name", newName);
            n.setProperty("dateOfBirth", dateOfBirth.getMillis());

            if (StringUtils.isNotBlank(email)) {
                n.setProperty("email", email);
            } else if (n.hasProperty("email")) {
                n.removeProperty("email");
            }

            if (StringUtils.isNotBlank(mobile)) {
                n.setProperty("mobile", mobile);
            } else if (n.hasProperty("mobile")) {
                n.removeProperty("mobile");
            }

            tx.success();
        }
    }

    public String createPerson(final String newName,
                               final DateTime dateOfBirth,
                               final String email,
                               final String mobile) {
        String newRef = new PersonRefGenerator().generateRef(newName, dateOfBirth);
        try (Transaction tx = db.beginTx()) {
            Node n = db.createNode();
            n.addLabel(NodeLabel.PERSON);
            n.setProperty("personRef", newRef);
            n.setProperty("name", newName);
            n.setProperty("dateOfBirth", dateOfBirth.getMillis());

            if (StringUtils.isNotBlank(email)) {
                n.setProperty("email", email);
            }

            if (StringUtils.isNotBlank(mobile)) {
                n.setProperty("mobile", mobile);
            }

            tx.success();
        }
        return newRef;
    }

    public List<Course> getCourses() {
        List<Course> courses = new ArrayList<Course>();

        try (Transaction ignored = db.beginTx()) {
            ResourceIterator<Node> ri = db.findNodes(NodeLabel.COURSE);
            while (ri.hasNext()) {
                Node currentNode = ri.next();

                Course tempCourse = new Course();
                tempCourse.setName((String) currentNode.getProperty("name"));
                if (currentNode.hasProperty("description")) {
                    tempCourse.setDescription((String) currentNode.getProperty("description"));
                }
                tempCourse.setCourseRef((String) currentNode.getProperty("courseRef"));

                courses.add(tempCourse);
            }
        }

        return courses;
    }

    public Course getCourse(String courseRef) {
        try (Transaction ignored = db.beginTx()) {
            Node n = db.findNode(NodeLabel.COURSE, "courseRef", courseRef);
            if (n == null) {
                return null;
            }

            Course tempCourse = new Course();
            tempCourse.setName((String) n.getProperty("name"));
            tempCourse.setCourseRef((String) n.getProperty("courseRef"));
            if (n.hasProperty("description")) {
                tempCourse.setDescription((String) n.getProperty("description"));
            }

            return tempCourse;
        }
    }

    public List<CourseTrainingRecord> getCourseTrainingRecords(final String courseRef) {
        List<CourseTrainingRecord> courseTrainingRecords = new ArrayList<CourseTrainingRecord>();

        try (Transaction ignored = db.beginTx()) {
            Node course = db.findNode(NodeLabel.COURSE, "courseRef", courseRef);
            if (course == null) {
                return null;
            }

            PersonFactory pf = new PersonFactory();
            for (Relationship singleRelationship : course.getRelationships(RelType.HAS)) {
                Node person = singleRelationship.getStartNode();

                // Filter out expired people
                if (singleRelationship.hasProperty("expires")) {
                    DateTime expires = new DateTime(singleRelationship.getProperty("expires"));
                    if (expires.isBeforeNow()) {
                        continue;
                    }
                }

                CourseTrainingRecord tr = new CourseTrainingRecord();
                tr.setPersonRef((String) person.getProperty("personRef"));
                tr.setPersonName((String) person.getProperty("name"));
                tr.setGained(new DateTime(singleRelationship.getProperty("gained")));
                tr.setExpires(new DateTime(singleRelationship.getProperty("expires")));
                tr.setCourseRef((String) course.getProperty("courseRef"));
                courseTrainingRecords.add(tr);
            }

            return courseTrainingRecords;
        }
    }

    public List<Person> getPeopleWithCourse(String courseRef) {
        List<Person> persons = new ArrayList<Person>();

        try (Transaction ignored = db.beginTx()) {
            Node course = db.findNode(NodeLabel.COURSE, "courseRef", courseRef);
            if (course == null) {
                return null;
            }

            PersonFactory pf = new PersonFactory();
            for (Relationship singleRelationShip : course.getRelationships(RelType.HAS)) {
                Node person = singleRelationShip.getStartNode();

                // Filter out expired people
                if (singleRelationShip.hasProperty("expires")) {
                    DateTime expires = new DateTime(singleRelationShip.getProperty("expires"));
                    if (expires.isBeforeNow()) {
                        continue;
                    }
                }

                persons.add(pf.getPerson(person));
            }

            return persons;
        }
    }

    public String createCourse(final String name,
                               final String description) {
        String newRef = new CourseRefGenerator().generateRef(name);
        try (Transaction tx = db.beginTx()) {
            Node n = db.createNode();
            n.addLabel(NodeLabel.COURSE);
            n.setProperty("courseRef", newRef);
            n.setProperty("name", name);
            if (StringUtils.isNotBlank(description)) {
                n.setProperty("description", description);
            }
            tx.success();
        }
        return newRef;
    }

    public void updateCourse(final String currentCourseRef,
                             final String name,
                             final String description) {
        try (Transaction tx = db.beginTx()) {
            Node n = db.findNode(NodeLabel.COURSE, "courseRef", currentCourseRef);
            if (n == null) {
                throw new RuntimeException("Cannot update a missing course");
            }

            n.setProperty("courseRef", new CourseRefGenerator().generateRef(name));
            n.setProperty("name", name);
            if (StringUtils.isNotBlank(description)) {
                n.setProperty("description", description);
            } else if (n.hasProperty("description")) {
                n.removeProperty("description");
            }
            tx.success();
        }
    }

    public void updatePersonTrainingRecords(final String personRef, List<PersonTrainingRecord> newRecords) {
        try (Transaction tx = db.beginTx()) {
            Node person = db.findNode(NodeLabel.PERSON, "personRef", personRef);
            if (person == null) {
                throw new RuntimeException("Missing person");
            }

            // Clear all the old relationships
            for (Relationship r : person.getRelationships(RelType.HAS)) {
                r.delete();
            }

            // Add the new ones
            for (PersonTrainingRecord singleRecord : newRecords) {
                Node course = db.findNode(NodeLabel.COURSE, "courseRef", singleRecord.getCourseRef());
                if (course == null) {
                    throw new RuntimeException("Missing course - " + singleRecord.getCourseTitle());
                }

                if (singleRecord.getGained() != null && singleRecord.getExpires() != null) {
                    Relationship r = person.createRelationshipTo(course, RelType.HAS);
                    r.setProperty("gained", singleRecord.getGained().getMillis());
                    r.setProperty("expires", singleRecord.getExpires().getMillis());
                }
            }
            tx.success();
        }
    }

    public void deletePerson(final String personRef) {
        try (Transaction tx = db.beginTx()) {
            Node person = db.findNode(NodeLabel.PERSON, "personRef", personRef);
            if (person == null) {
                throw new RuntimeException("Missing person");
            }

            for (Relationship r : person.getRelationships(RelType.HAS)) {
                r.delete();
            }
            person.delete();

            tx.success();
        }
    }

    public void deleteCourse(final String courseRef) {
        try (Transaction tx = db.beginTx()) {
            Node course = db.findNode(NodeLabel.COURSE, "courseRef", courseRef);
            if (course == null) {
                throw new RuntimeException("Missing course");
            }

            for (Relationship r : course.getRelationships(RelType.HAS)) {
                r.delete();
            }
            course.delete();

            tx.success();
        }
    }
}

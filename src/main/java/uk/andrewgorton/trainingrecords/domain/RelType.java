package uk.andrewgorton.trainingrecords.domain;

import org.neo4j.graphdb.RelationshipType;

public enum RelType implements RelationshipType {
    HAS
}

FROM java:openjdk-8-jre
MAINTAINER Andrew Gorton (noreply@andrewgorton.uk)

RUN mkdir -p /opt/trainingrecords
COPY ./target/TrainingDatabase-1.0.3-SNAPSHOT.jar /opt/trainingrecords/

RUN mkdir -p /mnt/trainingrecords-config
VOLUME /mnt/trainingrecords-config
EXPOSE 8080

ENTRYPOINT /usr/bin/java -jar /opt/trainingrecords/TrainingDatabase-1.0.3-SNAPSHOT.jar server /mnt/trainingrecords-config/config.yml
